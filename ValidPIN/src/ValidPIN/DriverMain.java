package ValidPIN;

public class DriverMain {

	public static void main(String[] args) {
		System.out.println(validate("121317"));
		System.out.println(validate("1234"));
		System.out.println(validate("45135"));
		System.out.println(validate("89abc1"));
		System.out.println(validate("900876"));
		System.out.println(validate(" 4983"));
		System.out.println(validate(""));
		System.out.println(validate(null));

	}

	private static boolean validate(String PIN) {
		if (PIN == null)
			return false;
		if (PIN.length()==4||PIN.length()==6) {
			return PIN.chars().allMatch(Character::isDigit);
		}
		return false;
	}
	
}
