package TextTwist;

import java.util.HashMap;

public class DriverMain {

	public static void main(String[] args) {
				
		System.out.println(totalPoints(new String[]{"cat", "create", "sat"}, "caster"));
		System.out.println(totalPoints(new String[]{"trance", "recant"}, "recant"));
		System.out.println(totalPoints(new String[]{"dote", "dotes", "toes", "set", "dot", "dots", "sted"}, "tossed"));

	}
	
	public static int totalPoints(String[] guessedWords, String answer) {
		int myPoints = 0;
		for(String word: guessedWords) {
			boolean validWord = true;
			HashMap<Character, Integer> answerMap = buildMap(answer);
			for(char letter: word.toCharArray()) {
				if(answerMap.containsKey(letter) && answerMap.get(letter)>=1) {
						answerMap.replace(letter, answerMap.get(letter)-1);
				}else
					validWord = false;
			}
			if(validWord==true) {
				myPoints+=(word.length()-2);
				if(word.length()==answer.length())
					myPoints+=50;
			}
		}
		
		return myPoints;
	}

	private static HashMap<Character, Integer> buildMap(String answer){
		HashMap<Character, Integer> answerMap = new HashMap<Character, Integer>();
		for(Character chr: answer.toCharArray()) {
			if(answerMap.containsKey(chr)) {
				answerMap.replace(chr, answerMap.get(chr)+1);
			}else {
				answerMap.put(chr, 1);
			}
		}
		return answerMap;
	}
}
