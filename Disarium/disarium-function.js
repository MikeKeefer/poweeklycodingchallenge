function isDisarium(n){
    var count = 0;
    var digitSum = 0;
    var digits = n.toString().split('').map(iNum => parseInt(iNum));
    for (elem of digits){
        count++;
        digitSum+=Math.pow(elem, count);
    }
    
    return n==digitSum;
}

console.log(isDisarium(75))
console.log(isDisarium(135))
console.log(isDisarium(518))
console.log(isDisarium(544))
console.log(isDisarium(8))
console.log(isDisarium(466))