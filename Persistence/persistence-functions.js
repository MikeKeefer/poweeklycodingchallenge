function additivePersistence(n){
    var count = 0;
    var number =n;
    while (number>10){
        count++;
        var digitSum = 0;
        var digits = number.toString().split('').map(iNum => parseInt(iNum,10));
        for (elem of digits){
            digitSum+=elem;
        }
        number = digitSum;
    }
        
    return count;
}

console.log("Additive Persistence")
console.log(additivePersistence(1679583));
console.log(additivePersistence(123456));
console.log(additivePersistence(0));


function multiplicativePersistence(n){
    var count = 0;
    var number =n;
    while (number>10){
        count++;
        var digitProduct = 1;
        var digits = number.toString().split('').map(iNum => parseInt(iNum,10));
        for (elem of digits){
            digitProduct*=elem;
        }
        number = digitProduct;
    }
        
    return count;
}

console.log("Multiplicative Persistence")
console.log(multiplicativePersistence(77));
console.log(multiplicativePersistence(123456));
console.log(multiplicativePersistence(4));
