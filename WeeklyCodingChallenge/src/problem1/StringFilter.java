package problem1;

import java.util.ArrayList;
import java.util.List;

public class StringFilter {

	public static void main(String[] args) {
		
		System.out.println(filterArray(new ArrayList<>(List.of(1,2, "a","b"))));
		System.out.println(filterArray(new ArrayList<>(List.of(1, "a","b", 0, 15))));
		System.out.println(filterArray(new ArrayList<>(List.of(1,2, "aasf","1", "123", 123))));

	}
	
	private static List<Integer> filterArray(List<?> inputArray){
		List<Integer> myArray = new ArrayList<Integer>();
		for (int i=0; i<=inputArray.size()-1; i++) {
			if(inputArray.get(i).getClass()==Integer.class) {
				myArray.add((Integer)inputArray.get(i));
			}
		}
		
		return myArray;
	}

}
