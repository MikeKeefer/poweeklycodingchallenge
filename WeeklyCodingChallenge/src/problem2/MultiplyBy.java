package problem2;

public class MultiplyBy {

	public static void main(String[] args) {
		System.out.println(multiplyBy11("11"));
		System.out.println(multiplyBy11("111111111"));
		System.out.println(multiplyBy11("1213200020"));
		System.out.println(multiplyBy11("1217197941"));
		System.out.println(multiplyBy11("9473745364784876253253263723"));

	}
	
	private static StringBuilder multiplyBy11(String inputString) {
		//Add zero to front and end
		char[] ten = new String("0").concat(inputString).concat("0").toCharArray();
		
		//Initial states
		StringBuilder answer = new StringBuilder ("");
		boolean carryOver=false;
		int adder = 0;
		
		//loop through charArray -> add chars together
		for (int i = ten.length-1; i>0; i--){
			adder = ten[i]+ten[i-1]-(48*2);
			
			if (carryOver==true) {
				adder+=1;
				carryOver=false;
			}
			
			if (adder>9) {
				adder-=10;
				carryOver=true;
			}
	
			answer.insert(0, String.valueOf(adder));
			
		}
		
		if(carryOver==true)
			answer.insert(0,  "1");
			
		return answer;
	}

}
